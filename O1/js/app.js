(function(){

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAhZMxYzbPxi8UPGAFSFS7KovpK1lAu7Yw",
    authDomain: "c7-beth.firebaseapp.com",
    databaseURL: "https://c7-beth.firebaseio.com",
    storageBucket: "c7-beth.appspot.com",
    messagingSenderId: "599134624021"
};


firebase.initializeApp(config);


var app = angular.module('app', ['firebase']);



app.controller('MainCtrl', function($scope, $firebaseObject){
	//firebase.database().ref().child('Operacion')//.on('value', snap => $scope.data = snap.val());
	
	

	const RootRef = firebase.database().ref();
	const Ref = RootRef.child('Operacion');

	this.db = $firebaseObject(Ref);
	


	$scope.generarOperacion = function(){

		var tituloOperacion = $scope.tituloOperacion;
		var descripcionOperacion = $scope.descripcionOperacion;
		var operadorOperacion = $scope.operadorOperacion;
		var claseOperacion  = $scope.claseOperacion;

		console.log
		("Titulo Operacion:" 
			+ tituloOperacion 
			+ " " 
			+ "Descripcion Operacion:"
			+ " "
			+ descripcionOperacion
			+ " "
			+ "Operador:"
			+ " "
			+ operadorOperacion
			+ " "
			+ "Clase Operacion:"
			+ " "
			+ claseOperacion);
		
		firebase.database().ref("/Operacion").push({
			"Titulo" : tituloOperacion,
			"Descripcion Operacion": descripcionOperacion,
			"Operador": operadorOperacion,
			"Clase Operacion" : claseOperacion,
			"Estado" : "Abierta"
		});
	};
});
}());

